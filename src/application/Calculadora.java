package application;

import java.util.Scanner;

public class Calculadora {
	
	public static void main(String[] args) {
		
	Scanner sc = new Scanner(System.in);
	
	System.out.print("Digite o primeiro numerador: ");
	int numerador1 = sc.nextInt();
	System.out.print("Digite o primeiro denominador: ");
	int denominador1 = sc.nextInt();
	System.out.print("Digite o segundo numerador: ");
	int numerador2 = sc.nextInt();
	System.out.print("Digite o segundo denominador: ");
	int denominador2 = sc.nextInt();
	int mmc = denominador1 * denominador2;
	int operacao1 = (mmc/denominador1)*numerador1;
	int operacao2 = (mmc/denominador2)*numerador2;
	
	if (denominador1 == denominador2) {
		System.out.println("Frações digitadas: " + numerador1 + "/" + denominador1 + " e " + numerador2 + "/" + denominador2);
		System.out.println("A soma das frações é: " + (numerador1 + numerador2) + "/" + denominador1);
		System.out.println("A subtração das frações é: " + (numerador1 - numerador2) + "/" + denominador1);
		System.out.println("A multiplicação das frações é: " + (numerador1 * numerador2) + "/" + (denominador1 * denominador2));
		System.out.println("A divisão das frações é: " + (numerador1 * denominador2) + "/" + numerador2 * denominador1);
	} else if (denominador1 != denominador2) {
		System.out.println("Frações digitadas: " + numerador1 + "/" + denominador1 + " e " + numerador2 + "/" + denominador2);
		System.out.println("A soma das frações é: " + (operacao1 + operacao2) + "/" + mmc);
		System.out.println("A subtração das frações é: " + (operacao1 - operacao2) + "/" + mmc);
		System.out.println("A multiplicação das frações é: " + (numerador1 * numerador2) + "/" + (denominador1 * denominador2));
		System.out.println("A divisão das frações é: " + (numerador1 * denominador2) + "/" + (numerador2 * denominador1));
	}
		
	sc.close();
		
	}
}