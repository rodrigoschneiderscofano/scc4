package application;

import java.util.Scanner;

public class SistemaMonetario {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
			
		int saque = sc.nextInt();
		int calc1 = saque/5;
		int calc2 = saque%5;
		int calc3 = calc2%3;
		int calc4 = calc2/3;
		int a1 = saque - 6;
		int a2 = a1%5;
		int a3 = a1/5;
		int a4 = 6/3;
		int b1 = saque - 9;		
		int b2 = b1%5;		
		int b3 = b1/5;		
		int b4 = 9/3;
		int d1 = saque - 12;		
		int d2 = d1%5;		
		int d3 = d1/5;		
		int d4 = 12/3;
				
		if (saque<=2 || saque==4 || saque==7) {
			System.out.println("Valor n�o pode ser sacado!");
		} else if (calc2 == 0) {
			System.out.println("Saque R$" + saque + ": " + calc1 + " nota(s) de R$5 e " + calc3 + " nota(s) de R$3");
		} else if (calc3 == 0) {
			System.out.println("Saque R$" + saque + ": " + calc1 + " nota(s) de R$5 e " + calc4 + " nota(s) de R$3");
		} else if (a2==0) {
			System.out.println("Saque R$" + saque + ": " + a3 + " nota(s) de R$5 e " + a4 + " nota(s) de R$3");
		} else if (b2==0) {
			System.out.println("Saque R$" + saque + ": " + b3 + " nota(s) de R$5 e " + b4 + " nota(s) de R$3");
		} else if (d2==0) {
			System.out.println("Saque R$" + saque + ": " + d3 + " nota(s) de R$5 e " + d4 + " nota(s) de R$3");
		} 
				
		sc.close();					
	}
}