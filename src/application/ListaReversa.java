package application;

import java.util.ArrayList;
import java.util.Collections;

public class ListaReversa {

	public static void main(String[] args) {
		
		//lista original: (25,35,40,21)
			
		ArrayList<Integer> reverseList = new ArrayList<Integer>();
		reverseList.add(25);
		reverseList.add(35);
		reverseList.add(40);
		reverseList.add(21);
		Collections.reverse(reverseList);
		System.out.print(reverseList);
					
	}
}	