package application;

public class VogaisConsoantes {

	public static void main(String[] args) {
		
		String s = "desafio";
		char[] Array = s.toCharArray();	
		char[] vogais = {'a','e','i','o','u'};
		char[] consoantes = {'d','s','f'};
		
		System.out.print("Vogais: ");
		for (int i=0; i<s.length(); i++) {			
			for (int j=0; j<vogais.length; j++) {
				if (Array[i] == vogais[j])
					System.out.print(vogais[j]);
			}
		}		
		
		System.out.println();	
		
		System.out.print("Consoantes: ");
		for (int i=0; i<s.length(); i++) {
			for (int j=0; j<consoantes.length; j++) {
				if (Array[i] == consoantes[j])
					System.out.print(consoantes[j]);
			}
		}
	}
}